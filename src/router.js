import Vue from 'vue'
import Router from 'vue-router'
import Home from './views/Homepage.vue'

Vue.use(Router)

export default new Router({
  mode: 'history',
  base: process.env.BASE_URL,
  routes: [
    {
      path: '/',
      name: 'home',
      component: Home
    },
    {
      path: '/proyectos',
      name: 'proyectos',
      // route level code-splitting
      // this generates a separate chunk (about.[hash].js) for this route
      // which is lazy-loaded when the route is visited.
      component: () => import(/* webpackChunkName: "proyectos" */ './views/Proyectos.vue')
    },
    {
      path: '/desarrollo',
      name: 'desarrollo',
      component: () => import(/* webpackChunkName: "desarrollo" */ './views/Desarrollo.vue')
    },
    {
      path: '/curriculum',
      name: 'curriculum',
      component: () => import(/* webpackChunkName: "desarrollo" */ './views/about/Curriculum.vue')
    },
    {
      path: '/cv',
      name: 'cv',
      component: () => import(/* webpackChunkName: "desarrollo" */ './views/CV.vue')
    },
    {
      path: '/redes',
      name: 'redes',
      component: () => import(/* webpackChunkName: "redes" */ './views/Redes.vue')
    },
    {
      path: '/servidores',
      name: 'servidores',
      component: () => import(/* webpackChunkName: "servidores" */ './views/Servidores.vue')
    },
    {
      path: '/quienes',
      name: 'quienes',
      component: () => import(/* webpackChunkName: "servidores" */ './views/Quienes.vue')
    }
  ]
})

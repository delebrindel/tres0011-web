import TresAboutMe from '@/components/tres0011/sections/AboutMe'
import TresWorkExperience from '@/components/tres0011/sections/WorkExperience'
import TresTechnicalSkills from '@/components/tres0011/sections/TechnicalSkills'
import TresAboutNavigation from '@/components/tres0011/sections/AboutNavigation'
import Hero from '@/components/bulma/Hero.vue'

export default {
	name: "Curriculum",
	data: function () {
		return {
			selector: "about",
			selectedLanguage: "eng"
		};
	},
	components: {
		TresAboutMe,
		TresWorkExperience,
		TresTechnicalSkills,
		TresAboutNavigation,
		Hero
	},
	methods: {
		openModal: function (ProjectInfo) {
			this.isActive = true;
			this.titulo = ProjectInfo.name;
			this.descripcion = ProjectInfo.desc;
			this.imagen = "assets/img/proyectos/" + ProjectInfo.img;
			this.status = ProjectInfo.status;
			this.link = ProjectInfo.link;
		},
		changeLanguage: function(newLanguage) {
			this.selectedLanguage = newLanguage;
		}
	},
};
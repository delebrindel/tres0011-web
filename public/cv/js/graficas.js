jQuery(document).ready(function($) {
	var ctx = $("#PHP");
	var myDoughnutChart = new Chart(ctx, {
		type: 'doughnut',
		data: {
			datasets:[{
				data:[90, 10],
				backgroundColor: [
				'rgb(0, 0, 160)',
				'rgb(22,192,232)']
			}] 
		},
		options: {
			legend: {
				display: false
			},
			responsive:true,
			maintainAspectRatio: false,
			tooltips: {
				callbacks: {
					label: function(tooltipItem) {
						return tooltipItem.yLabel;
					}
				}
			}
		}
	});
	var ctx = $("#HTML");
	var myDoughnutChart = new Chart(ctx, {
		type: 'doughnut',
		data: {
			datasets:[{
				data:[95, 5],
				backgroundColor: [
				'rgb(0, 0, 160)',
				'rgb(22,192,232)']
			}] 
		},
		options: {
			legend: {
				display: false
			},
			responsive:true,
			maintainAspectRatio: false,
			tooltips: {
				callbacks: {
					label: function(tooltipItem) {
						return tooltipItem.yLabel;
					}
				}
			}
		}
	});
	var ctx = $("#JS");
	var myDoughnutChart = new Chart(ctx, {
		type: 'doughnut',
		data: {
			datasets:[{
				data:[75, 25],
				backgroundColor: [
				'rgb(0, 0, 160)',
				'rgb(22,192,232)']
			}] 
		},
		options: {
			legend: {
				display: false
			},
			responsive:true,
			maintainAspectRatio: false,
			tooltips: {
				callbacks: {
					label: function(tooltipItem) {
						return tooltipItem.yLabel;
					}
				}
			}
		}
	});
	var ctx = $("#SQL");
	var myDoughnutChart = new Chart(ctx, {
		type: 'doughnut',
		data: {
			datasets:[{
				data:[85, 15],
				backgroundColor: [
				'rgb(0, 0, 160)',
				'rgb(22,192,232)']
			}] 
		},
		options: {
			legend: {
				display: false
			},
			responsive:true,
			maintainAspectRatio: false,
			tooltips: {
				callbacks: {
					label: function(tooltipItem) {
						return tooltipItem.yLabel;
					}
				}
			}
		}
	});
	var ctx = $("#NWK");
	var myDoughnutChart = new Chart(ctx, {
		type: 'doughnut',
		data: {
			datasets:[{
				data:[85, 15],
				backgroundColor: [
				'rgb(0, 0, 160)',
				'rgb(22,192,232)']
			}] 
		},
		options: {
			legend: {
				display: false
			},
			responsive:true,
			maintainAspectRatio: false,
			tooltips: {
				callbacks: {
					label: function(tooltipItem) {
						return tooltipItem.yLabel;
					}
				}
			}
		}
	});
	var ctx = $("#SER");
	var myDoughnutChart = new Chart(ctx, {
		type: 'doughnut',
		data: {
			datasets:[{
				data:[85, 15],
				backgroundColor: [
				'rgb(0, 0, 160)',
				'rgb(22,192,232)']
			}] 
		},
		options: {
			legend: {
				display: false
			},
			responsive:true,
			maintainAspectRatio: false,
			tooltips: {
				callbacks: {
					label: function(tooltipItem) {
						return tooltipItem.yLabel;
					}
				}
			}
		}
	});
});
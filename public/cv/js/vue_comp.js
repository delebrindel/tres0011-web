var app = new Vue({
	el: '#app',
	components: {
	  'home': httpVueLoader('js/components/index.vue'),
	  'educacion': httpVueLoader('js/components/educacion.vue')
	},
	data:{
		menu: 'educacion'
	},
	methods:{
	}
})
window.vue = app;